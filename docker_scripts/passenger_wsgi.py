import os, sys
sys.path = [
    '/home/app/webapp',
    '/home/app/webapp/bcscheduler',
] + sys.path

import django
import django.core.handlers.wsgi
from distutils.version import LooseVersion

if LooseVersion(django.get_version()) >= LooseVersion('1.7'):
    django.setup()

application = django.core.handlers.wsgi.WSGIHandler()
