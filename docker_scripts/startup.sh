#!/bin/bash
cd /home/app
setfacl -R -m d:u:app:rwx,u:app:rwx webapp
setfacl -R -m d:u:www-data:rwx,u:www-data:rwx webapp
setfacl -R -m d:u:root:rwx,u:root:rwx webapp

# Some initial preparations
source /etc/profile.d/webapp.sh
cd /home/app/webapp/bcscheduler
python3 manage.py collectstatic --noinput
python3 manage.py migrate
