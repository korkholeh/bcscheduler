# Hosted cron like microservice

Hosted backend for BC task scheduler.

## Require

Install required libraries

    sudo apt-get install build-essential g++ flex bison gperf ruby perl \
      libsqlite3-dev libfontconfig1-dev libicu-dev libfreetype6 libssl-dev \
      libpng-dev libjpeg-dev python libx11-dev libxext-dev

Install PhantomJS

    git clone git://github.com/ariya/phantomjs.git
    cd phantomjs
    git checkout 2.0
    ./build.sh

    sudo ln -s bin/phantomjs /usr/local/share/phantomjs
    sudo ln -s bin/phantomjs /usr/local/bin/phantomjs
    sudo ln -s bin/phantomjs /usr/bin/phantomjs

## Environment variables

* `DJANGO_SETTINGS_MODULE`
* `DATABASE_URL`
* `SECRET_KEY` - secret key (optional)
* `DEFAULT_FROM_EMAIL` - default address for notification emails
* `GHOST_DRIVER_LOG`

