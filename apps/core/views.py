from django.shortcuts import redirect
from django.core.urlresolvers import reverse


def home(request):
    if not request.user.is_authenticated():
        return redirect(reverse('login'))

    return redirect(reverse('jobs_list'))
