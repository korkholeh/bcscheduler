from django.contrib import admin
from .models import BCSite, BCJob


class BCSiteAdmin(admin.ModelAdmin):
    list_display = ('name', 'bc_subdomain')
    search_fields = ('name',)


class BCJobAdmin(admin.ModelAdmin):
    # fields = ('bc_site', 'app_key', 'last_ping',)
    list_display = (
        'bc_site', 'app_key', 'active', 'last_ping',)
    list_filter = ('bc_site',)

admin.site.register(BCSite, BCSiteAdmin)
admin.site.register(BCJob, BCJobAdmin)
