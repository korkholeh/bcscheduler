#!/bin/bash
{{job_prefix|safe}}
python3 {{project_path|safe}}/manage.py start_job {{job.id}}
{{job_suffix|safe}}