from django.conf.urls import patterns, url
from .views import (
    BCSiteListView,
    BCSiteDetailView,
    BCSiteCreateView,
    BCSiteDeleteView,

    BCJobListView,
    BCJobDetailView,
    BCJobCreateView,
    BCJobDeleteView,
)


urlpatterns = patterns(
    '',
    url(r'^sites/$', BCSiteListView.as_view(), name='sites_list'),
    url(r'^sites/add/$', BCSiteCreateView.as_view(), name='site_create'),
    url(
        r'^sites/(?P<pk>\d+)/$',
        BCSiteDetailView.as_view(), name='site_details'),
    url(
        r'^sites/(?P<pk>\d+)/delete/$',
        BCSiteDeleteView.as_view(), name='site_delete'),

    url(r'^jobs/$', BCJobListView.as_view(), name='jobs_list'),
    url(r'^jobs/add/$', BCJobCreateView.as_view(), name='job_create'),
    url(r'^jobs/(?P<pk>\d+)/$', BCJobDetailView.as_view(), name='job_details'),
    url(
        r'^jobs/(?P<pk>\d+)/delete/$',
        BCJobDeleteView.as_view(), name='job_delete'),
)
