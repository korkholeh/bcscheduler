from django import forms
from .models import BCSite


class BCSiteForm(forms.ModelForm):
    class Meta:
        model = BCSite
        fields = ['name', 'bc_subdomain', 'admin_user', 'admin_password']

    def __init__(self, *args, **kwargs):
        super(BCSiteForm, self).__init__(*args, **kwargs)
        self.fields['admin_password'].widget = forms.widgets.PasswordInput()
