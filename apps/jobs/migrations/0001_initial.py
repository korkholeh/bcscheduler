# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BCJob',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('active', models.BooleanField(verbose_name='Active', default=True)),
                ('button_id', models.CharField(verbose_name='Button Id', default='job-button', max_length=100)),
                ('cron_time', models.CharField(help_text='minute (0-59), hour (0-23, 0 = midnight), day (1-31), month (1-12), weekday (0-6, 0 = Sunday)', verbose_name='Cron time', default='0 0 * * *', max_length=200)),
                ('last_ping', models.DateTimeField(verbose_name='Last Ping', default=django.utils.timezone.now)),
                ('modified', models.DateTimeField(verbose_name='Modified', auto_now=True)),
                ('app_key', models.CharField(verbose_name='App Key', max_length=100)),
            ],
            options={
                'ordering': ('-last_ping',),
                'verbose_name': 'BC Job',
                'verbose_name_plural': 'BC Jobs',
            },
        ),
        migrations.CreateModel(
            name='BCSite',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('name', models.CharField(verbose_name='Name', max_length=255)),
                ('bc_subdomain', models.CharField(verbose_name='BC Subdomain', max_length=255)),
                ('admin_user', models.CharField(verbose_name='Admin User', max_length=255)),
                ('admin_password', models.CharField(verbose_name='Admin Password', max_length=255)),
            ],
            options={
                'ordering': ('name',),
                'verbose_name': 'Business Catalyst Site',
                'verbose_name_plural': 'Business Catalyst Sites',
            },
        ),
        migrations.AddField(
            model_name='bcjob',
            name='bc_site',
            field=models.ForeignKey(to='jobs.BCSite', verbose_name='BC Site', related_name='jobs'),
        ),
    ]
