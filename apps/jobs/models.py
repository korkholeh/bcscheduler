from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone


class BCSite(models.Model):
    name = models.CharField(_('Name'), max_length=255)
    bc_subdomain = models.CharField(_('BC Subdomain'), max_length=255)
    admin_user = models.CharField(_('Admin User'), max_length=255)
    admin_password = models.CharField(_('Admin Password'), max_length=255)

    class Meta:
        verbose_name = _('Business Catalyst Site')
        verbose_name_plural = _('Business Catalyst Sites')
        ordering = ('name',)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse(
            'site_details',
            kwargs={'pk': self.id})

    def get_secure_domain(self):
        return 'https://%s.worldsecuresystems.com' % self.bc_subdomain


class BCJobCronManager(models.Manager):
    def update_config(self):
        from django.template.loader import render_to_string
        import subprocess
        import os

        jobs = self.get_queryset().filter(active=True)
        # Save jobs to individual files
        for job in jobs:
            f = open(os.path.join(
                settings.CRON_SCRIPTS_DIR, 'job_%s.sh' % job.id), 'w')
            job_script = render_to_string(
                'jobs/cron_job.sh',
                {
                    'job': job,
                    'project_path': settings.BASE_DIR,
                    'job_prefix': settings.CRON_COMMAND_PREFIX,
                    'job_suffix': settings.CRON_COMMAND_SUFFIX,
                })
            f.write(job_script)
            f.close()

        cron_config = render_to_string(
            'jobs/cron_config.txt',
            {
                'jobs': jobs,
                'project_path': settings.BASE_DIR,
                'cron_scripts_dir': settings.CRON_SCRIPTS_DIR,
                'job_log': settings.CRON_COMMAND_LOG,
            })
        f = open(settings.TMP_CRON_FILE, 'w')
        f.write(cron_config)
        f.close()
        subprocess.call(['crontab', settings.TMP_CRON_FILE])
        os.remove(settings.TMP_CRON_FILE)


class BCJob(models.Model):
    bc_site = models.ForeignKey(
        BCSite, verbose_name=_('BC Site'), related_name='jobs')
    active = models.BooleanField(_(u'Active'), default=True)
    app_key = models.CharField(_('App Key'), max_length=100)
    button_id = models.CharField(
        _('Button Id'), max_length=100, default='job-button')
    cron_time = models.CharField(
        _(u'Cron time'), max_length=200, default='0 0 * * *',
        help_text=_(
            u'minute (0-59), hour (0-23, 0 = midnight), day (1-31), '
            u'month (1-12), weekday (0-6, 0 = Sunday)'))
    last_ping = models.DateTimeField(
        _('Last Ping'), default=timezone.now)
    modified = models.DateTimeField(_(u'Modified'), auto_now=True)

    objects = models.Manager()
    cron = BCJobCronManager()

    class Meta:
        verbose_name = _('BC Job')
        verbose_name_plural = _('BC Jobs')
        ordering = ('-last_ping',)

    def __str__(self):
        return '%s - %s' % (str(self.bc_site), self.app_key)

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse(
            'job_details',
            kwargs={'pk': self.id})

    def get_app_url(self):
        return '%s/Admin/AppLoader.aspx?client_id=%s' % (
            self.bc_site.get_secure_domain(),
            self.app_key)

    def run(self):
        from selenium import webdriver
        from selenium.common.exceptions import NoSuchElementException
        from selenium.webdriver.common.by import By
        from selenium.webdriver.support.ui import WebDriverWait
        from selenium.webdriver.support import expected_conditions as EC
        # print ('Run job')
        driver = webdriver.PhantomJS(
            service_log_path=settings.GHOST_DRIVER_LOG)
        driver.set_window_size(1120, 550)
        driver.get(self.get_app_url())
        # driver.save_screenshot('login.png')
        try:
            driver.find_element_by_id('txtLogin').send_keys(
                self.bc_site.admin_user)
            driver.find_element_by_id('txtPassword').send_keys(
                self.bc_site.admin_password)
            driver.find_element_by_id("lbtnLogin").click()
        except NoSuchElementException:
            print ('WARNING: Login window not found.')
        driver.switch_to_frame('hybrid')
        driver.find_element_by_id(self.button_id).click()
        # driver.save_screenshot('out.png')
        # print (driver.current_url)
        try:
            element = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.ID, "job-status-success")))
        finally:
            driver.find_element_by_id("job-status-success")
            driver.quit()


@receiver(post_save, sender=BCJob)
def bcjob_post_save(sender, **kwargs):
    BCJob.cron.update_config()
