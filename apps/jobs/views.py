from django.views.generic.list import ListView
from django.views.generic import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from .models import (
    BCSite,
    BCJob,
)
from .forms import BCSiteForm


class BCSiteListView(ListView):
    model = BCSite
    context_object_name = 'sites'


class BCSiteDetailView(UpdateView):
    model = BCSite
    form_class = BCSiteForm
    context_object_name = 'site'
    success_url = reverse_lazy('sites_list')


class BCSiteCreateView(CreateView):
    model = BCSite
    form_class = BCSiteForm
    success_url = reverse_lazy('sites_list')


class BCSiteDeleteView(DeleteView):
    model = BCSite
    success_url = reverse_lazy('sites_list')


class BCJobListView(ListView):
    model = BCJob
    context_object_name = 'jobs'


class BCJobDetailView(UpdateView):
    model = BCJob
    fields = [
        'bc_site', 'active', 'app_key', 'button_id',
        'cron_time']
    context_object_name = 'job'
    success_url = reverse_lazy('jobs_list')


class BCJobCreateView(CreateView):
    model = BCJob
    fields = [
        'bc_site', 'active', 'app_key', 'button_id',
        'cron_time']
    success_url = reverse_lazy('jobs_list')


class BCJobDeleteView(DeleteView):
    model = BCJob
    success_url = reverse_lazy('jobs_list')
