from django.core.management.base import BaseCommand
from ...models import BCJob


class Command(BaseCommand):
    help = 'Update cron config'

    def handle(self, *args, **options):
        BCJob.cron.update_config()
