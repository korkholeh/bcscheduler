from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone
from ...models import BCJob


class Command(BaseCommand):
    help = 'Start cron job'

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('job_id', nargs='+', type=int)

    def handle(self, *args, **options):
        for job_id in options['job_id']:
            job = BCJob.objects.filter(pk=job_id)
            if job.exists():
                self.stdout.write('Start job "%s"' % job_id)
                job[0].run()
                job.update(last_ping=timezone.now())
                self.stdout.write('Finish job "%s"' % job_id)
            else:
                raise CommandError('Job %s does not exist.' % job_id)
