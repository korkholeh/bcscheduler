# Settings for development environment
from .base import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG

SECRET_KEY = 'qwertyuiop123456'

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025
