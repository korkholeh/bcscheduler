# Settings for development environment
from .base import *

DEBUG = False
TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = ['*']

MEDIA_ROOT = '/home/app/webapp/public/media'
STATIC_ROOT = '/home/app/webapp/public/static'
